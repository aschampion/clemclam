import os
from typing import Dict, List, Optional
import webbrowser

import PySide2
from magicgui import magicgui, magic_factory
import napari
from napari.qt import progress
import numpy as np

from clemclam.data import (
    CACHE,
    ActivityMapDataset,
    ClemClamProject,
    DynamicCombinedMap,
    MapChannels,
    cached_n5_read,
    read_min_max_from_am,
)

from .anim import AnimWidget

try:
    from IPython import get_ipython

    if get_ipython():
        get_ipython().run_line_magic("gui", "qt")
except ImportError:
    pass


COLORMAPS = [
    "viridis",
    "plasma",
    "inferno",
    "magma",
    "cividis",
    "PiYG",
    "PRGn",
    "BrBG",
    "PuOr",
    "RdGy",
    "RdBu",
    "RdYlBu",
    "RdYlGn",
    "Spectral",
    "coolwarm",
    "bwr",
    "seismic",
]


def combined_to_ldt(combined, scale, prefix="") -> List[napari.types.LayerDataTuple]:
    colormap = ["red", "green", "blue"]
    return [
        (
            combined[:, :, :, i],
            {
                "colormap": c,
                "scale": scale,
                "name": f"{prefix}Activity Map ({c})",
                "contrast_limits": [0.0, 1.0],
                "blending": "additive",
            },
        )
        for (i, c) in enumerate(colormap)
    ]
    # Napari LDTs don't understand `Viewer.add_image`'s multichannel arguments.
    # return (combined, {'channel_axis':-1, 'colormap':colormap, 'scale':scale, 'name':[f"Activity Map ({c})" for c in colormap]})


def add_combined_to_nap(combined, viewer, scale):
    colormap = ["red", "green", "blue"]
    return viewer.add_image(
        combined,
        channel_axis=-1,
        colormap=colormap,
        scale=scale,
        name=[f"Activity Map ({c})" for c in colormap],
    )


def dwidget_prefix(id):
    return f"{id}: "


def copy_to_clipboard(text):
    cb = PySide2.QtGui.QGuiApplication.clipboard()
    cb.setText(text)


class ClemClamProjectGui:
    proj: ClemClamProject

    def __init__(self, proj: ClemClamProject):
        self.proj = proj

    def create_viewer(self):
        viewer = napari.Viewer(ndisplay=3)
        self.setup_viewer(viewer)
        viewer.window.qt_viewer.activityDock.show()
        # Make this project instance accessible from the built-in console.
        viewer.window._qt_window.qt_viewer.console.push({"ccpg": self})
        return viewer

    def project_widget(self, viewer):
        from magicgui.widgets import ComboBox, Container, PushButton, Label, LineEdit

        widgets = []

        widgets.append(Label(name=f"Sample: {self.proj.name}"))

        def list_select(name, choices, value, setter, include_none=False):
            # magicgui handles None values magically, which is to say not well.
            choices = [(r["name"], i) for (i, r) in enumerate(choices)]
            if include_none:
                choices += [("None", -1)]
            v = value
            if v is None:
                v = -1
            select = ComboBox(name=name, choices=choices, value=v)

            def set_reg(r):
                if r < 0:
                    r = None
                setattr(self.proj, setter, r)

            select.changed.connect(lambda self, event=None: set_reg(self.value))
            return select

        # TTC
        if "ttc" in self.proj.conf["em"]:
            select = list_select(
                "tissue mask",
                self.proj.conf["em"]["ttc"],
                self.proj.selected_ttc,
                "selected_ttc",
            )
            widgets.append(select)

        # Nuclei
        if "nuclei" in self.proj.conf["em"]:
            select = list_select(
                "nuclei",
                self.proj.conf["em"]["nuclei"],
                self.proj.selected_nuclei,
                "selected_nuclei",
            )
            widgets.append(select)

            def load_em_nuclei(self, viewer):
                nuclei_conf = self.proj.conf["em"]["nuclei"][self.proj.selected_nuclei]
                n5_path = os.path.join(self.proj.root_path, nuclei_conf["path"])
                n5_dataset = nuclei_conf["display_scale_dataset"]
                nuclei = cached_n5_read(n5_path, n5_dataset)
                if "ttc" in self.proj.conf["em"]:
                    nuclei = self.proj.mask_by_ttc(nuclei)
                viewer.add_image(
                    nuclei.astype(np.int8),
                    scale=self.proj.em_res,
                    name=f"EM Nuclei ({nuclei_conf['name']})",
                    visible=False,
                    opacity=0.5,
                    colormap="cyan",
                )

            button = PushButton(text="Load EM nuclei detections")
            button.changed.connect(lambda _: load_em_nuclei(self, viewer))
            widgets.append(button)

        # Registrations
        select = list_select(
            "registration",
            self.proj.conf["registrations"],
            self.proj._selected_reg,
            "selected_reg",
            include_none=True,
        )
        widgets.append(select)

        # Datasets
        choices = self.proj.datasets.keys()
        dataset_select = ComboBox(name="dataset", choices=choices)
        widgets.append(dataset_select)

        load_dataset = PushButton(text="Open widget for selected dataset")
        load_dataset.changed.connect(
            lambda _: viewer.window.add_dock_widget(
                ActivityMapDatasetGui(
                    self.proj.datasets[dataset_select.value]
                ).dataset_widget(),
                name=f"Dataset {dataset_select.value} ({ActivityMapDatasetGui._next_dwidget_id - 1})",
            )
        )
        widgets.append(load_dataset)

        open_metadata = PushButton(text="View metadata for selected dataset")
        open_metadata.changed.connect(
            lambda _: webbrowser.open(
                self.proj.datasets[dataset_select.value].metadata_url,
            )
        )
        widgets.append(open_metadata)

        # Mean activity
        def load_am(self, viewer):
            layer = self.load_and_warp_am_to_nap(
                viewer, self.proj.datasets[dataset_select.value]
            )
            cl = layer.contrast_limits
            cl[0] = 102.0
            layer.contrast_limits = cl
            layer.opacity = 0.5
            layer.colormap = "green"

        button = PushButton(text="Load mean activity")
        button.changed.connect(lambda _: load_am(self, viewer))
        widgets.append(button)

        other_image = LineEdit()
        widgets.append(other_image)

        def load_other(self, viewer, image_path):
            image = self.proj.warp_image(image_path)
            viewer.add_image(image, scale=self.proj.warp_res, blending="additive")

        button = PushButton(text="Load other image")
        button.changed.connect(lambda _: load_other(self, viewer, other_image.value))
        widgets.append(button)

        button = PushButton(text="Clear cache")
        button.changed.connect(lambda _: CACHE.clear())
        widgets.append(button)

        button = PushButton(text="Open animation widget")
        button.changed.connect(
            lambda _: viewer.window.add_dock_widget(
                AnimWidget().widget(), name="Animation"
            )
        )
        widgets.append(button)

        container = Container(widgets=widgets)

        return container

    def setup_viewer(self, viewer):
        layer = self.load_em_n5_to_nap(viewer)
        # layer.contrast_limits[0] = 223.0

        @viewer.mouse_drag_callbacks.append
        def callback(layer, event):
            coord = np.flip(np.asarray(viewer.cursor.position))
            # coord *= self.em_res
            copy_to_clipboard(str(",".join(str(x) for x in coord)))

        @viewer.bind_key("/")
        def go_to_coordinates(event):
            cb = PySide2.QtGui.QGuiApplication.clipboard()
            coord = map(float, cb.text().split(","))
            coord = np.asarray(list(coord))
            # coord /= self.em_res
            coord = np.flip(coord)
            b = viewer.dims.order[0]
            c = list(viewer.dims.current_step)
            c[b] = coord[b] / np.flip(self.proj.em_res)[b]
            viewer.dims.current_step = tuple(c)
            viewer.camera.center = tuple(coord[x] for x in viewer.dims.order)

        viewer.window.add_dock_widget(
            self.project_widget(viewer), name="CLEMCLAM Project"
        )

    def load_em_n5_to_nap(self, viewer, ttc_mask=True) -> napari.layers.Layer:
        n5_path = os.path.join(self.proj.root_path, self.proj.conf["em"]["path"])
        n5_dataset = self.proj.conf["em"]["display_scale"]["dataset"]
        em = cached_n5_read(n5_path, n5_dataset)
        if ttc_mask and "ttc" in self.proj.conf["em"]:
            masked_em = self.proj.mask_by_ttc(em)
            viewer.add_image(
                masked_em,
                scale=self.proj.em_res,
                name=f"Masked EM ({self.proj.name})",
                rendering="attenuated_mip",
            )
        return viewer.add_image(
            em,
            scale=self.proj.em_res,
            name=f"EM ({self.proj.name})",
            visible=not ttc_mask,
        )

    def load_and_warp_am_to_nap(
        self, viewer, dataset: Optional[ActivityMapDataset] = None
    ):
        if dataset is None:
            dataset = next(iter(self.proj.datasets))
        filename = dataset.lsm_mean_path()
        am = self.proj.warp_image(filename)
        return viewer.add_image(am, scale=self.proj.warp_res, name=f"LSM Mean (warped)")


class ActivityMapDatasetGui:
    _next_dwidget_id = 0
    dataset: ActivityMapDataset

    def __init__(self, dset):
        self.dataset = dset
        self.opened_combined = {}  # Map from dwidget_id to map widget

    def _params_from_magicgui(self, **kwargs):
        params = {k: kwargs[k] for k, _ in self.dataset.parameters() if k in kwargs}
        for k, v in self.dataset.experiment_type_params.items():
            params[k] = v["from_magicgui"](kwargs[k])

        return params

    def _widget(
        self,
        viewer,
        dwidget_id,
        map_channels=MapChannels.DynamicCombined,
        colormap="viridis",
        mask_map_by_tissue=False,
        **kwargs,
    ) -> List[napari.types.LayerDataTuple]:

        params = self._params_from_magicgui(**kwargs)

        if dwidget_id in self.opened_combined:
            widget = self.opened_combined.pop(dwidget_id)
            widget.close()

        if map_channels == MapChannels.DynamicCombined:
            combined = DynamicCombinedMap(
                self.dataset, mask_map_by_tissue, colormap, params, progress=progress
            )
            combined_gui = DynamicCombinedMapGui(combined, dwidget_id)
            self.opened_combined[dwidget_id] = viewer.window.add_dock_widget(
                combined_gui.widget(),
                name=f"{dwidget_prefix(dwidget_id)}Map {combined.name}",
            )
            return combined_to_ldt(
                combined.combine(progress=progress),
                self.dataset.proj.warp_res,
                prefix=dwidget_prefix(dwidget_id),
            )
        elif map_channels == MapChannels.PreCombined:
            with progress(total=2) as pbr:
                file = self.dataset.activity_map_path(
                    map_channels_string=map_channels.value, **params
                )
                pbr.set_description(f"Warping {MapChannels.PreCombined.name}")
                combined = self.dataset.proj.warp_image(file)
                pbr.update(1)
                pbr.set_description(f"Splitting to layers")
                layers = combined_to_ldt(
                    combined,
                    self.dataset.proj.warp_res,
                    prefix=dwidget_prefix(dwidget_id),
                )
                pbr.update(1)
                return layers
        else:
            with progress(total=2) as pbr:
                file = self.dataset.activity_map_path(
                    map_channels_string=map_channels.value, **params
                )
                pbr.set_description(f"Warping {map_channels.name}")
                am = self.dataset.proj.warp_image(file)
                pbr.update(1)
                pbr.set_description(f"Reading limits")
                c_range = read_min_max_from_am(file)
                pbr.update(1)
                if mask_map_by_tissue:
                    am = self.dataset.proj.mask_by_ttc(am, mask_value=c_range[0])
            return [
                (
                    am,
                    {
                        "colormap": colormap,
                        "contrast_limits": c_range,
                        "scale": self.dataset.proj.warp_res,
                        "name": f"{dwidget_prefix(dwidget_id)}Activity Map ({map_channels.name})",
                        "blending": "additive",
                    },
                )
            ]

    def dataset_widget(self):
        dwidget_id = ActivityMapDatasetGui._next_dwidget_id
        ActivityMapDatasetGui._next_dwidget_id += 1

        magic_kwargs = {
            k: v["magicgui_options"]
            for k, v in self.dataset.experiment_type_params.items()
        }
        magic_kwargs.update(
            {
                "colormap": {"choices": COLORMAPS},
                "dwidget_id": {"visible": False},
                "call_button": "Load Map",
            }
        )

        for k, v in self.dataset.parameters():
            magic_kwargs[k] = {"choices": sorted(v)}

        # magicgui requires parameters are named in function def, which requires this dynamic creation.
        context = globals().copy()
        context["self"] = self
        exec(
            f"""
def wrapped_widget(
    viewer: napari.Viewer,
    {",".join(k for k, _ in self.dataset.experiment_type_params.items())},
    {",".join(k for k, _ in self.dataset.parameters())},
    dwidget_id={dwidget_id},
    map_channels=MapChannels.DynamicCombined,
    colormap="viridis",
    mask_map_by_tissue=False,
) -> List[napari.types.LayerDataTuple]:
    return self._widget(viewer, dwidget_id, map_channels, colormap, mask_map_by_tissue,
        {",".join(f"{k}={k}" for k, _ in self.dataset.experiment_type_params.items())},
        {",".join(f"{k}={k}" for k, _ in self.dataset.parameters())})
        """,
            context,
        )

        magic_widget = magicgui(context["wrapped_widget"], **magic_kwargs)

        from magicgui.widgets import PushButton

        # Parameter string
        button = PushButton(text="Copy parameter string")

        def parameter_string(self, widget):
            # Replace with `FunctionGui::asdict` after upgrade to `magicgui` 0.3.
            kwargs = {
                w.name: getattr(w, "value", None)
                for w in widget
                if w.name and not w.gui_only
            }
            params = self._params_from_magicgui(**kwargs)
            return (
                self.dataset.key
                + "/"
                + self.dataset.activity_map_filename(
                    map_channels_string=MapChannels.PValues.value, **params
                )
            )

        button.changed.connect(
            lambda _: copy_to_clipboard(parameter_string(self, magic_widget))
        )
        magic_widget.append(button)

        # Count CSVs
        def trans_csv(self, widget, prefix):
            # Replace with `FunctionGui::asdict` after upgrade to `magicgui` 0.3.
            kwargs = {
                w.name: getattr(w, "value", None)
                for w in widget
                if w.name and not w.gui_only
            }
            params = self._params_from_magicgui(**kwargs)
            return self.dataset.lsm_path(
                self.dataset.parameterized_filename(
                    prefix=prefix,
                    extension="csv",
                    map_channels_string=MapChannels.PValues.value,
                    **params,
                )
            )

        button = PushButton(text="Subjects per transition CSV")
        button.changed.connect(
            lambda _: webbrowser.open(
                trans_csv(self, magic_widget, prefix="n_subjs_per_trans")
            )
        )
        magic_widget.append(button)

        button = PushButton(text="Transition counts CSV")
        button.changed.connect(
            lambda _: webbrowser.open(trans_csv(self, magic_widget, prefix="n_trans"))
        )
        magic_widget.append(button)

        return magic_widget


class DynamicCombinedMapGui:
    map: DynamicCombinedMap

    def __init__(
        self,
        map: DynamicCombinedMap,
        dwidget_id,
    ):
        self.map = map
        self.dwidget_id = dwidget_id

    def widget(self) -> List[napari.types.LayerDataTuple]:
        @magic_factory(call_button="Reload")
        def _widget(
            p_min, p_max, log2_p_gamma, c_min, c_max, log2_c_gamma, colormap
        ) -> List[napari.types.LayerDataTuple]:
            p_range = [p_min, p_max]
            c_range = [c_min, c_max]
            return combined_to_ldt(
                self.map.combine(
                    p_range,
                    c_range,
                    np.exp2(log2_p_gamma),
                    np.exp2(log2_c_gamma),
                    colormap,
                    progress=progress,
                ),
                self.map.dataset.proj.warp_res,
                prefix=dwidget_prefix(self.dwidget_id),
            )

        GAMMA_RANGE = 3.0
        return _widget(
            p_min={
                "widget_type": "FloatSlider",
                "min": self.map.p_lim[0] * 1.1,
                "max": self.map.p_lim[1] * 1.1,
                "value": self.map.p_range[0],
                "label": "log p min",
            },
            p_max={
                "widget_type": "FloatSlider",
                "min": self.map.p_lim[0] * 1.1,
                "max": self.map.p_lim[1] * 1.1,
                "value": self.map.p_range[1],
                "label": "log p max",
            },
            log2_p_gamma={
                "widget_type": "FloatSlider",
                "min": -GAMMA_RANGE,
                "max": GAMMA_RANGE,
                "value": self.map.log2_p_gamma,
                "label": "log2 p gamma",
            },
            c_min={
                "widget_type": "FloatSlider",
                "min": self.map.c_lim[0] * 1.1,
                "max": self.map.c_lim[1] * 1.1,
                "value": self.map.c_range[0],
                "label": "coeff min",
            },
            c_max={
                "widget_type": "FloatSlider",
                "min": self.map.c_lim[0] * 1.1,
                "max": self.map.c_lim[1] * 1.1,
                "value": self.map.c_range[1],
                "label": "coeff max",
            },
            log2_c_gamma={
                "widget_type": "FloatSlider",
                "min": -GAMMA_RANGE,
                "max": GAMMA_RANGE,
                "value": self.map.log2_c_gamma,
                "label": "log2 c gamma",
            },
            colormap={"choices": COLORMAPS},
        )
