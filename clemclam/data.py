from collections import OrderedDict
from enum import Enum
from functools import lru_cache
import io
import itertools
import json
import logging
import os
from typing import Dict, List
from urllib.parse import urljoin, urlparse

from diskcache import Cache
import matplotlib
import matplotlib.cm
import numpy as np
import pandas as pd
import pyn5
import requests
import skimage
import skimage.io
import skimage.exposure
import skimage.transform
import tifffile
from tqdm import tqdm
import yaml

log = logging.getLogger(__name__)

TTC_LABELS = {
    "BACKGROUND": 0,
    "TISSUE": 1,
    "NEUROPIL": 2,
}

BEHAVIORS = {
    "Grouped": "G",
    "Unknown": "U",
    "Backwards": "B",
    "Fowards": "F",
    "Hunch": "H",
    "Quiet": "Q",
    "Turn Combined": "TC",
    "Turn Left": "TL",
    "Turn Right": "TR",
}


class MapChannels(Enum):
    PValues = "_p_vls"
    Coefficients = "_coefs"
    DynamicCombined = ""
    PreCombined = "_combined"


_SESSION = requests.Session()
_SESSION.auth = (
    os.environ.get("CATMAID_HTTP_USER", ""),
    os.environ.get("CATMAID_HTTP_PASS", ""),
)

CACHE = Cache(
    directory="clemclam-cache",
    size_limit=int(10e9),
    eviction_policy="least-frequently-used",
)


@CACHE.memoize()
def cached_url(url):
    return _SESSION.get(url)


@lru_cache
def cached_skimage_imread(filename):
    uri = urlparse(filename)
    if uri.scheme == "" or uri.scheme == "file":
        image = skimage.io.imread(filename)
    else:
        r = cached_url(filename)
        # Necessary to specify tifffile plugin, otherwise ND tiffs are opened
        # as 2D when not given as a filename.
        image = skimage.io.imread(io.BytesIO(r.content), plugin="tifffile")

    log.info("Loaded %s with shape %s", filename, image.shape)

    return image


@lru_cache
def cached_n5_read(container, dataset):
    em = pyn5.File(container)[dataset]
    return em[:, :, :]


def uri_exists(filename):
    uri = urlparse(filename)
    if uri.scheme == "" or uri.scheme == "file":
        return os.path.exists(filename)
    else:
        return bool(_SESSION.head(filename))


def uri_read(filename, func):
    uri = urlparse(filename)
    if uri.scheme == "" or uri.scheme == "file":
        with open(filename) as file:
            result = func(file)
        return result
    else:
        r = cached_url(filename)
        return func(io.BytesIO(r.content))


def uri_filename_or_buffer(filename, func):
    uri = urlparse(filename)
    if uri.scheme == "" or uri.scheme == "file":
        return func(filename)
    else:
        r = cached_url(filename)
        return func(io.BytesIO(r.content))


def warp_field_to_warp_map(warp_field, moving_res, warp_res, warp_scale_nm):
    """Converts a warp field, which is a local offset vector field in the moving image space, into a voxel coordinate space absolute position warp map."""
    shape = warp_field.shape
    res_scale = warp_res / moving_res
    coords = np.mgrid[: shape[0], : shape[1], : shape[2]]
    coords = np.array([coords[i] * res_scale[i] for i in range(3)])
    # Reorder channels of warp field from xyz-like to zyx-like
    warp_field = np.flip(warp_field, axis=-1)
    # The warp field is in some physical unit. Convert to voxel offsets.
    warp_field *= warp_scale_nm / moving_res
    # Move the channels (corresponding to axis offsets) to the first axis.
    warp_field = np.rollaxis(warp_field, 3)
    coords = coords + warp_field
    return coords


def warp_image(moving, warp_map):
    return skimage.transform.warp(moving, warp_map)


def read_min_max_from_am(filename):
    t = uri_filename_or_buffer(filename, tifffile.TiffFile)
    msv = t.pages[0].tags["ImageDescription"]
    js = json.loads(msv.value)
    s_min, s_max = js["SuggestedMinSampleValue"], js["SuggestedMaxSampleValue"]
    if s_max < s_min:
        print(
            f"Warning: image suggested min is greater than max, swapping ({s_min}, {s_max}, {filename}"
        )
        s_min, s_max = s_max, s_min
    return (s_min, s_max)


def rgb_combined_from_pc(
    p_vls, coefs, p_range, c_range, p_gamma=1.0, c_gamma=1.0, colormap="viridis"
):
    cm = matplotlib.cm.get_cmap(colormap)

    def normalize(x, vmin, vmax):
        nx = np.clip(x, vmin, vmax)
        nx -= vmin
        if vmax == vmin:
            nx = np.ones_like(nx)
        else:
            nx *= 1.0 / (vmax - vmin)
        return nx

    combined = cm(normalize(coefs, c_range[0], c_range[1]))
    combined = np.delete(combined, -1, -1)
    combined = skimage.exposure.adjust_gamma(combined, c_gamma)

    alpha = 1 - normalize(p_vls, p_range[0], p_range[1])
    alpha = np.clip(alpha, 0.0, 1.0)
    alpha = alpha ** p_gamma

    combined = combined * np.expand_dims(alpha, axis=-1)
    return combined


class ClemClamProject:
    name: str
    root_path = ""
    _selected_reg = None

    def __init__(self, root_path, conf_yaml: Dict):
        self.name = conf_yaml["sample"]["name"]
        self.root_path = root_path
        self.conf = conf_yaml
        self.em_res = np.asarray(self.conf["em"]["res"]) * (
            2 ** np.asarray(self.conf["em"]["display_scale"]["scale"])
        )
        self.datasets = {
            n: ActivityMapDataset(
                n,
                self,
                am["path"],
                am["metadata"],
                am.get("experiment_type", None),
            )
            for n, am in self.conf["activity_maps"].items()
        }
        self.selected_reg = len(self.conf["registrations"]) - 1
        self.selected_ttc = len(self.conf["em"]["ttc"]) - 1
        self.selected_nuclei = len(self.conf["em"]["nuclei"]) - 1

    @classmethod
    def from_yaml(cls, path):
        root_path = os.path.join(os.path.abspath(os.path.join(path, os.pardir)), "")
        with open(path, "r") as file:
            conf = yaml.safe_load(file)
            return cls(root_path, conf)

    @property
    def raw_lsm_res(self):
        return np.asarray(self.conf["lsm"]["res"])

    @property
    def norm_lsm_res(self):
        perm = np.asarray(self.conf["lsm"]["to_em_perm"])
        scale = self.raw_lsm_res
        scale = [scale[i] for i in perm]
        return np.asarray(scale)

    @property
    def warp_res(self):
        if self.selected_reg is None:
            return self.norm_lsm_res
        else:
            return np.asarray(
                self.conf["registrations"][self.selected_reg]["warp_stack_res"]
            )

    @property
    def warp_scale_nm(self):
        if self.selected_reg is None:
            return 1000.0
        else:
            return self.conf["registrations"][self.selected_reg]["warp_scale_nm"]

    def warp_image(self, filename):
        if self.selected_reg is None:
            return self.load_lsm_to_normalized_space(filename)
        else:
            key = f"warped-{self._selected_reg_warp_path()}-{filename}"
            if key in CACHE:
                return CACHE[key]
            else:
                # Some warp fields assume the LSM is already in a normalized space
                # with approximately the same axis orientation.
                if self.conf["registrations"][self.selected_reg]["from_normalized_lsm"]:
                    image = self.load_lsm_to_normalized_space(filename)
                else:
                    image = cached_skimage_imread(filename)

                if image.ndim == 3:
                    image = skimage.transform.warp(image, self.warp_map)
                else:
                    chan = image.shape[3]
                    # new_image = np.zeros(list(self.warp_map.shape)[1:] + [chan,], dtype=image.dtype)
                    # for x in range(chan):
                    #     new_image[:, :, :, x] = skimage.transform.warp(image[:, :, :, x], self.warp_map)
                    # image = new_image
                    image = np.stack(
                        list(
                            skimage.transform.warp(image[:, :, :, x], self.warp_map)
                            for x in range(chan)
                        ),
                        axis=-1,
                    )
                    # warp_map = np.concatenate(self.warp_map, np.tile(np.expand_dims(range(chan), [1,2,3]), self.warp_map.shape[1:]))
                    # image = skimage.transform.warp(image, warp_map)

                CACHE[key] = image
                return image

    @property
    def selected_reg(self):
        return self._selected_reg

    def _selected_reg_warp_path(self):
        return os.path.join(
            self.root_path,
            self.conf["registrations"][self.selected_reg]["warp_stack_path"],
        )

    @selected_reg.setter
    def selected_reg(self, selected_reg):
        if selected_reg != self.selected_reg:
            self._selected_reg = selected_reg
            if self.selected_reg is not None:
                if self.conf["registrations"][self.selected_reg]["from_normalized_lsm"]:
                    lsm_res = self.norm_lsm_res
                else:
                    lsm_res = self.raw_lsm_res
                path = self._selected_reg_warp_path()
                self.warp_field = cached_skimage_imread(path)
                self.warp_map = warp_field_to_warp_map(
                    self.warp_field,
                    lsm_res,
                    self.warp_res,
                    self.warp_scale_nm,
                )

    def mask_by_ttc(self, image, mask_value=0):
        ttc_conf = self.conf["em"]["ttc"][self.selected_ttc]
        n5_path = os.path.join(self.root_path, ttc_conf["path"])
        n5_dataset = ttc_conf["display_scale_dataset"]
        ttc = cached_n5_read(n5_path, n5_dataset)
        masked = np.copy(image)
        masked[ttc == TTC_LABELS["BACKGROUND"]] = mask_value
        return masked

    def lsm_to_normalized_space(self, lsm):
        perm = self.conf["lsm"]["to_em_perm"]
        perm = perm + list(range(len(perm), lsm.ndim))  # Account for channel dimensions
        perm = np.asarray(perm)
        lsm = np.transpose(lsm, axes=perm)
        lsm = np.flip(lsm, axis=np.asarray(self.conf["lsm"]["to_em_flip"]))
        return lsm

    def load_lsm_to_normalized_space(self, filename):
        lsm = cached_skimage_imread(filename)
        return self.lsm_to_normalized_space(lsm)


class ActivityMapDataset:
    proj: ClemClamProject

    def __init__(self, key, project, path, metadata_filename, experiment_type):
        self.key = key
        self.proj = project
        self.uri = urljoin(self.proj.root_path, path)
        self.metadata_filename = metadata_filename
        self.experiment_type = experiment_type

        self.metadata = uri_read(self.metadata_url, yaml.safe_load)
        self.opened_combined = {}  # Map from dwidget_id to map widget

    @property
    def metadata_url(self):
        return urljoin(self.uri, self.metadata_filename)

    @property
    def experiment_type_params(self):
        def behavior_choices(b):
            return {"choices": [k for k, v in BEHAVIORS.items() if v in b]}

        def beh_to_magicgui(b):
            return behavior_choices(self.metadata[b])

        def magicgui_to_beh(b):
            return BEHAVIORS[b]

        params = OrderedDict()

        if self.experiment_type == "stimulus_behaviors":
            params["behavior_sequence"] = {
                "options": ["beh_before", "beh"],
                "magicgui_options": {"choices": ["Before", "After"]},
                "from_magicgui": lambda s: {"Before": "beh_before", "After": "beh"}[s],
            }
            params["behavior"] = {
                "options": self.metadata["suceeding_behaviors"],
                "magicgui_options": beh_to_magicgui("suceeding_behaviors"),
                "from_magicgui": magicgui_to_beh,
            }
        else:
            params["preceding_behavior"] = {
                "options": self.metadata["preceding_behaviors"],
                "magicgui_options": beh_to_magicgui("preceding_behaviors"),
                "from_magicgui": magicgui_to_beh,
            }
            params["succeeding_behavior"] = {
                "options": self.metadata["suceeding_behaviors"],
                "magicgui_options": beh_to_magicgui("suceeding_behaviors"),
                "from_magicgui": magicgui_to_beh,
            }

        return params

    def experiment_type_prefix(self, **kwargs):
        prefix = [kwargs[k] for k in self.experiment_type_params.keys()]
        return "_".join(prefix)

    def parameterized_filename(self, extension="tiff", **kwargs):
        # Parameters are listed in the metadata in their order in the filename.
        params_fmt = [k for k, _ in self.parameters()]
        params_fmt = (
            "{prefix}_" + "_".join(f"{k}_{{{k}}}" for k in params_fmt) + ".{extension}"
        )
        return str.format(params_fmt, extension=extension, **kwargs)

    def activity_map_filename(self, map_channels_string, **kwargs):
        prefix = self.experiment_type_prefix(**kwargs)
        prefix = "_".join([prefix, map_channels_string])
        return self.parameterized_filename(prefix=prefix, **kwargs)

    def activity_map_path(self, *args, **kwargs):
        return self.lsm_path(self.activity_map_filename(*args, **kwargs))

    def lsm_path(self, filename):
        return urljoin(self.uri, filename)

    def lsm_mean_path(self):
        params = {k: v[0] for k, v in self.parameters()}
        return self.lsm_path(self.parameterized_filename(prefix="mean", **params))

    def parameters(self):
        def leaves(d):
            for k, v in d.items():
                if isinstance(v, dict):
                    yield from leaves(v)
                else:
                    yield (k, v)

        for k, v in leaves(self.metadata["parameters"]):
            if isinstance(v, list):
                yield (k, v)

    def complete_parameters(self):
        params = dict(self.parameters())
        for k, v in self.experiment_type_params.items():
            params[k] = v["options"]
        return params

    def extant_parameters(self, map_channels=MapChannels.Coefficients, progress=tqdm):
        params = self.complete_parameters()
        values = list(params.values())
        n = np.prod([len(v) for v in values])
        extant = []
        with progress(total=n) as pbr:
            for s in itertools.product(*values):
                s_params = {k: v for (k, v) in zip(params.keys(), s)}
                file = self.activity_map_path(
                    map_channels_string=map_channels.value, **s_params
                )
                if uri_exists(file):
                    s = list(s)
                    s.append(file)
                    extant.append(s)
                pbr.update(1)

        df = pd.DataFrame(extant, columns=list(params.keys()) + ["uri"])
        return df


class DynamicCombinedMap:
    def __init__(
        self,
        dataset: ActivityMapDataset,
        mask_map_by_tissue,
        colormap,
        params,
        progress=tqdm,
    ):
        self.dataset = dataset
        self.colormap = colormap
        self.mask_map_by_tissue = mask_map_by_tissue
        self.name = self.dataset.activity_map_filename(
            map_channels_string=MapChannels.PValues.value, **params
        )

        with progress(total=3) as pbr:
            p_file = self.dataset.activity_map_path(
                map_channels_string=MapChannels.PValues.value, **params
            )
            c_file = self.dataset.activity_map_path(
                map_channels_string=MapChannels.Coefficients.value, **params
            )
            pbr.set_description(f"Warping {MapChannels.PValues.name}")
            self.p_vls = self.dataset.proj.warp_image(p_file)
            self.p_vls.flags.writeable = False
            pbr.update(1)
            pbr.set_description(f"Warping {MapChannels.Coefficients.name}")
            self.coefs = self.dataset.proj.warp_image(c_file)
            self.coefs.flags.writeable = False
            pbr.update(1)
            pbr.set_description(f"Reading limits")
            self.p_range = read_min_max_from_am(p_file)
            self.p_lim = [
                min(np.nanmin(self.p_vls), self.p_range[0], self.p_range[1]),
                max(np.nanmax(self.p_vls), self.p_range[1], self.p_range[0]),
            ]
            self.log2_p_gamma = 0.0
            self.c_range = read_min_max_from_am(c_file)
            self.c_lim = [
                min(np.nanmin(self.coefs), self.c_range[0]),
                max(np.nanmax(self.coefs), self.c_range[1]),
            ]
            self.log2_c_gamma = 0.0
            pbr.update(1)

    def combine(
        self,
        p_range=None,
        c_range=None,
        p_gamma=1.0,
        c_gamma=1.0,
        colormap=None,
        progress=tqdm,
    ):
        if p_range is None:
            p_range = self.p_range
        if c_range is None:
            c_range = self.c_range
        if colormap is None:
            colormap = self.colormap

        with progress(total=1) as pbr:
            if self.mask_map_by_tissue:
                p_vls = self.dataset.proj.mask_by_ttc(self.p_vls, mask_value=p_range[1])
            else:
                p_vls = self.p_vls
            pbr.set_description("Rendering combined image")

            combined = rgb_combined_from_pc(
                p_vls, self.coefs, p_range, c_range, p_gamma, c_gamma, colormap=colormap
            )
            pbr.update(1)
        return combined
