import argparse
from .clemclam import ClemClamProjectGui
from clemclam.data import ClemClamProject
import napari


def main():
    parser = argparse.ArgumentParser(
        description="Correlative Light-Electron Microscopy CATMAID-Linked Activity Maps"
    )

    parser.add_argument("project_file", default="project.yml")

    args = parser.parse_args()

    ccpg = ClemClamProjectGui(ClemClamProject.from_yaml(args.project_file))

    ccpg.create_viewer()
    napari.run()


if __name__ == "__main__":
    main()
